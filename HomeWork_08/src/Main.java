
public class Main {
    public static void main(String[] args) {
        int[] array = {12, 64, 4, 2, 100, 40, 56};


        ArrayTask resolve3 = (array1, from1, to1) -> {
            int i = from1, right = 0, left = 0, max = 0, localMax = max;
            while (i <= to1) {
                left = max;
                max = right;
                right = array[i++];
                if (right < max && max > left) {
                    System.out.println("local max: \n" + max);
                    localMax = max;
                }
            }
            int num = localMax;
            int sum = 0;
            while (num > 0) {
                sum = sum + num % 10;
                num = num / 10;
            }
            System.out.println("Sum of max element number :  ");
            return sum;
        };

        ArraysTasksResolver.resolveTask(array, resolve3, 3, 5);
    }
}
