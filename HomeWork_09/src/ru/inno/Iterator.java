package ru.inno;

public interface Iterator<T> {

    T next();

    boolean hasNext();
}

