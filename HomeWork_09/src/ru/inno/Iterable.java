package ru.inno;


public interface Iterable<T> {
    Iterator<T> iterator();
}
