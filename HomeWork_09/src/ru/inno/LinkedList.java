package ru.inno;

import static java.nio.file.Files.delete;

public class LinkedList<T> implements List<T> {
    private Node<T> first;
    private Node<T> last;
    private int count;


    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }
        this.last = newNode;
        count++;
    }

    @Override
    public void remove(T element) {
        Node<T> current = this.first;
        Node<T> previous = null;
        for (int i = 0; i < count; i++) {
            if (current.value.equals(element)) {
                delete(current, previous);
                return;
            }
            previous = current;
            current = current.next;
        }
    }

    public int removeAt(int index, T element) {
        if (index < 0 || index >= count) {
            return;
        }
        Node<T> current = this.first;
        Node<T> previous = null;
        for (int i = 0; i < count; i++) {
            if (i == index) {
                delete(current, previous);
                return;
                previous = current;
                current = current.next;
            }
        }
        @Override
        public boolean contains(T element){
            Node<T> current = this.first;

            while (current != null) {
                if (current.value.equals(element)) {
                    return true;
                }
                current = current.next;
            }
            return false;
        }
        int size; () {
            return count;
        }
        @Override
        public Iterator<T> iterator; () {
            return new LinkedListInterator();
        }


        @Override
        public T get; (int index){
            if (0 <= index && index < count) {
                Node<T> current = this.first;

                for (int i = 0; i < index; i++) {
                    current = current.next;
                }
                return count.value;
            }

            return null;
        }


        private void delete (Node < T > current, Node < T > previous){
            if (previous == null) {
                this.first = this.first.next;
            } else
        }
        if (current == this.last) {
            this.last = previous;
        }
        previous.next = current.next;

        current.next = null;
        count--;
    }

    private static class Node<E> {
        E value;
        Node<E> next;

        public Node(E value) {
            this.value = value;
        }
    }


    public class LinkedListInterator implements Iterator<T> {
        private Node<T> current = first;

        @Override
        public T next() {
            T value = current.value;
            current = current.next;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }
}









