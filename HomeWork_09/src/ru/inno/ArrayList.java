package ru.inno;
public class ArrayList<T> implements List<T> {
    private final static int DEFAULT_ARRAY_SIZE = 10;
    private T[] elements;
    private int count;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    @Override
    public void add(T element) {
        if (isFull()) {
            resize();
        }

        elements[count] = element;
        count++;
    }

    @Override
    public void remove(T element) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) {
                removeAt(i);
            }
        }
    }

    @Override
    public void removeAt(int index) {
        for (int i = index; i < count - 1; i++) {
            elements[i] = elements[i + 1];
        }

        elements[count - 1] = null;
        count--;
    }

    @Override
    public boolean cotains(T element) {
        for (int i = 0; i < count; i++) {
            if (elements[i].equals(element)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int size() {return count;}


    private class ArrayListIterator implements Iterator<T> {

        private int currentIndex = 0;

        @Override
        public T next() {
            T value = elements[currentIndex];
            currentIndex++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return currentIndex < count;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }
}








