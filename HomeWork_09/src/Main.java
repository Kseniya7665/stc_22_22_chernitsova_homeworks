import ru.inno.ArrayList;
import ru.inno.LinkedList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> integerArrayList = new ArrayList<>();

        integerArrayList.add(1);
        integerArrayList.add(2);
        integerArrayList.add(3);
        integerArrayList.add(4);
        integerArrayList.add(5);
        integerArrayList.add(6);

        System.out.println("\nArrayList");

        printCollection(integerArrayList);

        integerArrayList.removeAt(2);
        integerArrayList.remove(1);
        integerArrayList.remove(6);
        integerArrayList.add(7);
        integerArrayList.add(8);

        printCollection(integerArrayList);

        LinkedList<Integer> integerLinkedList = new LinkedList<>();

        integerLinkedList.add(1);
        integerLinkedList.add(2);
        integerLinkedList.add(3);
        integerLinkedList.add(4);
        integerLinkedList.add(5);
        integerLinkedList.add(6);

        System.out.println("\nLinkedList");

        integerLinkedList.add(7);
        integerLinkedList.add(8);

        printcollection(integerLinkedList);

        integerLinkedList.remove(2);

        
    }



}