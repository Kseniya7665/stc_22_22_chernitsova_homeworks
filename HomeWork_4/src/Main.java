import java.util.Random;
import java.util.Scanner;

public class Main {
    public static int calcSumOfArrayRange(int[] a, int from, int to) {
        int sum = 0;
        for (int i = from; i <= to; i++) {
            sum += a[i];
        }
        return sum;
    }

    public static boolean isFunction(int sum) {
        return sum != 0;
    }

    public static void printEvanNumber(int a[], int i) {
        if (a[i] % 2 == 0) {
            System.out.println(a[i]);
        }
    }

    public static void toInt(int[] array, int result) {
        for (int i = array.length - 1, n = 0; i >= 0; --i, n++) {
            int pos = (int) Math.pow(10, i);
            result += array[n] * pos;
        }
        System.out.println(result);
    }

    public static void toInt(int[] array, String result) {
        for (int i = 0; i < array.length; i++) {
            result += array[i];
        }
        System.out.println(result);
    }

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите ширину массива");
        int Leught = scanner.nextInt();
        System.out.println("Введите массив");
        int[] array = new int[Leught];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10);
            printEvanNumber(array, i);
        }
        int sum = calcSumOfArrayRange(array, 0, array.length - 1);
        if (isFunction(sum)) {
            System.out.println("Сумма " + sum);
        } else {
            System.out.println(" - 1 ");
        }
        toInt(array, "");
        toInt(array, 0);
    }
}