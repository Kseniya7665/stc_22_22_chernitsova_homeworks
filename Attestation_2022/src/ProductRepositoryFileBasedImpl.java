import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductRepositoryFileBasedImpl implements ProductRepository {

    private final String fileName;

    public ProductRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();
        Reader fileReader = null;
        BufferedReader bufferedReader = null;


        try {
            fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);

            String currentProducts = reader.readLine();
            while (currentProducts != null) {
                String[] parts = currentProducts.split("\\|");


                Integer id = Integer.parseInt(parts[0]);
                String nameProduct = parts[1];
                Double cost = Double.valueOf(parts[2]);
                Integer inStock = Integer.parseInt(parts[3]);

                Product product = new Product(id, nameProduct, cost, inStock);
                products.add(product);
                currentProducts = reader.readLine();
            }

        } catch (IOException e) {
            throw new UncuccesfulWorkWhihFileExeption(e);
        }finally {
            if (bufferedReader != null) {
                bufferedReader.close();

            }
            if (fileReader != null) {
                fileReader.close();
            }
        }
        System.out.println(products);
        return products;
    }
}