import javax.imageio.IIOException;

public class ProductService {
    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public int countOffAllProducts() {
        return productRepository.findAll().size();


    }

}
