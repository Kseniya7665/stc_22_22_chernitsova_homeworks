public class Main {
    public static void main(String[] args) {
        ProductRepository productRepository = new ProductRepositoryFileBasedImpl("list.txt");
        ProductService productService = new ProductService(productRepository);

        System.out.println(productService.countOffAllProducts());

    }
}