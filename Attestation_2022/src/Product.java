public class Product {
    private Integer id;
    private String nameProduct;
    private Double cost;
    private Integer inStock;

    public Product(Integer id, String nameProduct, Double cost, Integer inStock) {
        this.id = id;
        this.nameProduct = nameProduct;
        this.cost = cost;
        this.inStock = inStock;
    }

    public Integer getId() {
        return id;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public Double getCost() {
        return cost;
    }

    public Integer getInStock() {
        return inStock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", nameProduct='" + nameProduct + '\'' +
                ", cost=" + cost +
                ", inStock=" + inStock +
                '}';
    }
}
