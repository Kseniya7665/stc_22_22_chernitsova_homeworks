package com.figures;

import static java.lang.Math.PI;


public class Ellipse extends Figur{
    private double smallRadius;
    private double largeRadius;

    public Ellipse(double centerX, double centerY, double smallRadius, double largeRadius) {
        super(centerX, centerY);
        this.smallRadius = smallRadius;
        this.largeRadius = largeRadius;
    }

    @Override
    protected double getPerimetr() {
        return PI * (getLargeRadius() + getSmallRadius());
    }

    @Override
    protected double getArea() {
        return PI * (getLargeRadius() * getSmallRadius());
    }


    public double getSmallRadius() {
        return smallRadius;
    }

    public void setSmallRadius(int smallRadius) {
        this.smallRadius = smallRadius;
    }

    public double getLargeRadius() {
        return largeRadius;
    }

    public void setLargeRadius(int largeRadius) {
        this.largeRadius = largeRadius;
    }
}
