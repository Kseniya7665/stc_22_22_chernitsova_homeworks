package com.figures;


public class Rectangle extends Figur {

    private double height;
    private double length;

    public Rectangle(double centerX, double centerY, double height, double length) {
        super(centerX, centerY);
        this.height = height;
        this.length = length;
    }


    @Override
    protected double getPerimetr() {
        return (getHeight() * getLength()) * 2;
    }

    @Override
    protected double getArea() {
        return getHeight() * getLength();
    }


    public double getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
