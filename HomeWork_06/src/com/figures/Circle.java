package com.figures;

public class Circle extends Ellipse{

    public Circle(double centerX, double centerY, double smallRadius) {
        super(centerX, centerY, smallRadius, smallRadius);
    }
}
