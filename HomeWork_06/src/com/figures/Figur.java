package com.figures;


public abstract class Figur {

    protected double centerX;
    protected double centerY;

    public Figur(double centerX, double centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }

    protected abstract double getPerimetr();
    protected abstract double getArea();
    protected void move(int toX, int toY) {
        setCenterX(toX);
        setCenterY(toY);
    }

    public double getCenterX() {
        return centerX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }
}

