package com.figures;

public class Main {
    public static void main(String[] args) {
        Circle r = new Circle(2, 3, 4);
        Ellipse e = new Ellipse(3, 5, 12, 54);
        System.out.printf("Ellipse area: %.2f", e.getArea());
    }

}