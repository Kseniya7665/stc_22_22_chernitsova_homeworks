import java.io.IOException;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        ProductsRepostory productsRepostory = new ProductsRepostory("list.txt") {
            @Override
            public List<Product> findProductById(Integer id) {
                return null;
            }

            @Override
            public List<Product> findAll() {
                return null;
            }
        };
        System.out.println(productsRepostory.findProductById(5));
}
}