import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collector;
import java.io.BufferedReader;
import java.util.stream.Collectors;


public aclass ProductsRepositoryFileBasedImp implements ProductsRepostory {
    private final String fileName;

    public ProductsRepositoryFileBasedImp(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapping = currentProduct -> {
        String[] parts = currentProduct.split("\\|");
        Integer id = Integer.parseInt(parts[0]);
        String nameProduct = parts[1];
        Double cost = Double.parseDouble(parts[2]);
        Integer inStock = Integer.parseInt(parts[3]);
        return new Product(id, nameProduct, cost, inStock);
    };


    public List<Product> finedProductById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName))
                    .lines()
                    .map(stringToProductMapping)
                    .filter(product -> product.getId() == id)
                    .collect(Collectors.toList());
        }catch (IOException e) {
            throw new UncuccesfulWorkWithFileExeption(e);
        }
    }

    @Override
    public List<Product> findProductById(Integer id) {
        try {
            return new BufferedReader(new FileReader(fileName));

        }catch (IOException e) {
            throw new UncuccesfulWorkWithFileExeption(e);
        }
    }
}
