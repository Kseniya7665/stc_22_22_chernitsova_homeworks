import java.util.List;

public interface ProductsRepostory {
    List <Product> findProductById (Integer id);
    List <Product> findAll();
}
