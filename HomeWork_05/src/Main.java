import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ATM atm = new ATM(1000,500,3000);


        while (true) {
            System.out.println("Что будем делать?\n Нажмите 1 для снятия\n Нажмите 2 для пополнения\n Нажмите 0 для завершения");
                System.out.println("Выберите действие");
                Scanner scanner = new Scanner(System.in);
                int numbers = scanner.nextInt();

                if (numbers == 1) {
                    System.out.println("Введите сумму для получения");
                    System.out.println("Получено: " + atm.getMoney(scanner.nextInt()));
                    continue;
            }
            if (numbers == 2) {
                System.out.println("Введите сумму внесения");
                System.out.println("Внесено: " + atm.putOnMoney(scanner.nextInt()));
                continue;
            }
            if (numbers == 0) {
                System.out.println("Хорошего дня!");
                break;
            }
        }


    }
}