
public class ATM {
    private int sumOfLeastMoney;
    private int maxOutCash;
    private int maxCapacity;
    private int operationsCount;

    public ATM(int sumOfLeastMoney, int maxOutCash, int maxCapacity) {
        if (sumOfLeastMoney > maxCapacity) {
            throw new RuntimeException();
        }


        this.sumOfLeastMoney = sumOfLeastMoney;
        this.maxOutCash = maxOutCash;
        this.maxCapacity = maxCapacity;

    }
    public int putOnMoney (int incomeMoney) {
        if (getSumOfLeastMoney() + incomeMoney > getMaxCapacity()) {
            int valueToRetern = getSumOfLeastMoney() + incomeMoney - getMaxCapacity();
            setSumOfLeastMoney(getMaxCapacity());
            incrementOperations();
            return valueToRetern;
        }
        setSumOfLeastMoney(getSumOfLeastMoney() + incomeMoney);
        incrementOperations();
        return 0;
    }
    public int getMoney(int moneyGet) {
        if (getMaxOutCash() <= moneyGet) {
            System.out.println("Максимально возможная сумма для снятия 500");
            setSumOfLeastMoney(getSumOfLeastMoney() - getMaxOutCash());
            incrementOperations();
            return getMaxOutCash();
        }

        if(getSumOfLeastMoney() < moneyGet) {
            int valueToReturn = getSumOfLeastMoney() - getMaxCapacity();
            setSumOfLeastMoney(getSumOfLeastMoney() - getMaxOutCash());
            incrementOperations();
            return valueToReturn;
        }
        int valueToReturn = getSumOfLeastMoney();
        setSumOfLeastMoney(0);
        incrementOperations();
        return valueToReturn;
    }
    private void incrementOperations() {
        operationsCount++;
    }

    public void setSumOfLeastMoney(int sumOfLeastMoney) {
        this.sumOfLeastMoney = sumOfLeastMoney;
    }

    public int getSumOfLeastMoney() {
        return sumOfLeastMoney;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public int getMaxOutCash() {
        return maxOutCash;
    }
}

