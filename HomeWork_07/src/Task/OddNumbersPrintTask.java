package Task;

public class OddNumbersPrintTask extends NumbersPrintTask {
    public OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    public void complete() {
        for (int i = getFrom(); i <= getTo(); i++) {
            if (i % 2 != 0) System.out.println(i + " ");
        }
    }
}
